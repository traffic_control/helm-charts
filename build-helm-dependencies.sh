#!/bin/sh

for d in */ ; do
    cd $d
    helm dependency build
    cd ..
done