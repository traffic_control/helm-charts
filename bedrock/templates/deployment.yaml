apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "bedrock.fullname" . }}
  labels:
    {{- include "bedrock.labels" . | nindent 4 }}
spec:
{{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
{{- end }}
  selector:
    matchLabels:
      {{- include "bedrock.selectorLabels" . | nindent 6 }}
  {{- if .Values.strategy }}
  strategy:
    {{- toYaml .Values.strategy | nindent 4 }}
  {{- end }}
  template:
    metadata:
    {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      labels:
        {{- include "bedrock.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
            - name: DB_PREFIX
              value: {{ .Values.db.prefix | default "wp_" | quote }}
            - name: DB_HOST
              value: {{ include "bedrock.databaseHost" . | quote }}
            - name: DB_NAME
              value: {{ include "bedrock.databaseName" . | quote }}
            - name: DB_USER
              value: {{ include "bedrock.databaseUser" . | quote }}
            - name: DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ include "bedrock.databaseSecretName" . }}
                  key: mariadb-password
            - name: WP_HOME
              {{- if or .Values.ingress.enableCertManager .Values.ingress.tls }}
              value: "https://{{ include "bedrock.fqn" . }}"
              {{- else }}
              value: "http://{{ include "bedrock.fqn" . }}"
              {{- end }}
            - name: WP_SITEURL
              {{- if or .Values.ingress.enableCertManager .Values.ingress.tls }}
              value: "https://{{ include "bedrock.fqn" . }}/wp"
              {{- else }}
              value: "http://{{ include "bedrock.fqn" . }}/wp"
              {{- end }}
            - name: AUTH_KEY
              valueFrom:
                secretKeyRef:
                  name: {{ include "bedrock.secretName" . }}
                  key: auth-key
            - name: SECURE_AUTH_KEY
              valueFrom:
                secretKeyRef:
                  name: {{ include "bedrock.secretName" . }}
                  key: secure-auth-key
            - name: LOGGED_IN_KEY
              valueFrom:
                secretKeyRef:
                  name: {{ include "bedrock.secretName" . }}
                  key: logged-in-key
            - name: NONCE_KEY
              valueFrom:
                secretKeyRef:
                  name: {{ include "bedrock.secretName" . }}
                  key: nonce-key
            - name: AUTH_SALT
              valueFrom:
                secretKeyRef:
                  name: {{ include "bedrock.secretName" . }}
                  key: auth-salt
            - name: SECURE_AUTH_SALT
              valueFrom:
                secretKeyRef:
                  name: {{ include "bedrock.secretName" . }}
                  key: secure-auth-salt
            - name: LOGGED_IN_SALT
              valueFrom:
                secretKeyRef:
                  name: {{ include "bedrock.secretName" . }}
                  key: logged-in-salt
            - name: NONCE_SALT
              valueFrom:
                secretKeyRef:
                  name: {{ include "bedrock.secretName" . }}
                  key: nonce-salt
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
          {{- if .Values.probes }}
          {{- if .Values.probes.livenessProbe }}
          livenessProbe:
            {{- if .Values.probes.livenessProbe.httpGet }}
            httpGet:
              httpHeaders:
                {{- if .Values.probes.livenessProbe.httpGet.httpHeaders }}
                {{- toYaml .Values.probes.livenessProbe.httpGet.httpHeaders | nindent 16 }}
                {{- end }}
                - name: "Host"
                  value: {{ include "bedrock.fqn" . | quote }}
              {{- range $key, $values := .Values.probes.livenessProbe.httpGet }}
              {{- if not (eq $key "httpHeaders") }}
              {{ $key }}: {{ toYaml $values | nindent 16 }}
              {{- end }}
            {{- end }}
            {{- range $key, $values := .Values.probes.livenessProbe }}
            {{- if not (eq $key "httpGet") }}
            {{ $key }}: {{ toYaml $values | nindent 14 }}
            {{- end }}
            {{- end }}
            {{- else }}
            {{- toYaml .Values.probes.livenessProbe | nindent 12 }}
            {{- end }}
          {{- end }}
          {{- if .Values.probes.startupProbe }}
          startupProbe:
            {{- if .Values.probes.startupProbe.httpGet }}
            httpGet:
              httpHeaders:
                {{- if .Values.probes.startupProbe.httpGet.httpHeaders }}
                {{- toYaml .Values.probes.startupProbe.httpGet.httpHeaders | nindent 16 }}
                {{- end }}
                - name: "Host"
                  value: {{ include "bedrock.fqn" . | quote }}
              {{- range $key, $values := .Values.probes.startupProbe.httpGet }}
              {{- if not (eq $key "httpHeaders") }}
              {{ $key }}: {{ toYaml $values | nindent 16 }}
              {{- end }}
            {{- end }}
            {{- range $key, $values := .Values.probes.startupProbe }}
            {{- if not (eq $key "httpGet") }}
            {{ $key }}: {{ toYaml $values | nindent 14 }}
            {{- end }}
            {{- end }}
            {{- else }}
            {{- toYaml .Values.probes.startupProbe | nindent 12 }}
            {{- end }}
          {{- end }}
          {{- if .Values.probes.readinessProbe }}
          readinessProbe:
            {{- if .Values.probes.readinessProbe.httpGet }}
            httpGet:
              httpHeaders:
                {{- if .Values.probes.readinessProbe.httpGet.httpHeaders }}
                {{- toYaml .Values.probes.readinessProbe.httpGet.httpHeaders | nindent 16 }}
                {{- end }}
                - name: "Host"
                  value: {{ include "bedrock.fqn" . | quote }}
              {{- range $key, $values := .Values.probes.readinessProbe.httpGet }}
              {{- if not (eq $key "httpHeaders") }}
              {{ $key }}: {{ toYaml $values | nindent 16 }}
              {{- end }}
            {{- end }}
            {{- range $key, $values := .Values.probes.readinessProbe }}
            {{- if not (eq $key "httpGet") }}
            {{ $key }}: {{ toYaml $values | nindent 14 }}
            {{- end }}
            {{- end }}
            {{- else }}
            {{- toYaml .Values.probes.readinessProbe | nindent 12 }}
            {{- end }}
          {{- end }}
          {{- end }}
          volumeMounts:
            - mountPath: /var/www/bedrock/web/app/uploads
              name: bedrock-data
              subPath: bedrock
            {{ if and (or (.Files.Glob "files/php.ini") .Values.phpIniConfiguration) (not .Values.configurationConfigMap) }}
            - mountPath: /usr/local/etc/php/conf.d/php.ini
              name: bedrock-config
              subPath: php.ini
            {{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      volumes:
        - name: bedrock-data
          {{- if .Values.persistence.enabled }}
          persistentVolumeClaim:
            claimName: {{ .Values.persistence.existingClaim | default (include "bedrock.fullname" .) }}
          {{- else }}
          emptyDir: {}
          {{ end }}
        {{ if and (or (.Files.Glob "files/php.ini") .Values.phpIniConfiguration) (not .Values.configurationConfigMap) }}
        - name: bedrock-config
          configMap:
            name: {{ template "bedrock.configurationCM" . }}
        {{- end }}
