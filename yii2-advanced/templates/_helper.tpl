{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "yii2Advanced.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "yii2Advanced.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "yii2Advanced.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Return the yii2-advanced TLS secret name
*/}}
{{- define "yii2Advanced.certManagerSecretName" -}}
{{- if ( not .Values.existingCertManagerSecretName) }}
    {{- printf "%s-cert-manager" (include "yii2Advanced.fullname" .) -}}
{{- else -}}
    {{- printf "%s" .Values.existingCertManagerSecretName -}}
{{- end -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "yii2Advanced.labels" -}}
helm.sh/chart: {{ include "yii2Advanced.chart" . }}
{{ include "yii2Advanced.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Return  the proper Storage Class
*/}}
{{- define "yii2Advanced.storageClass" -}}
{{/*
Helm 2.11 supports the assignment of a value to a variable defined in a different scope,
but Helm 2.9 and 2.10 does not support it, so we need to implement this if-else logic.
*/}}
{{- if and .persistence .persistence.storageClass -}}
    {{- if (eq "-" .persistence.storageClass) -}}
        {{- printf "storageClassName: \"\"" -}}
    {{- else }}
        {{- printf "storageClassName: %s" .persistence.storageClass -}}
    {{- end -}}
{{- end -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "yii2Advanced.selectorLabels" -}}
app.kubernetes.io/name: {{ include "yii2Advanced.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "mariadb.fullname" -}}
{{- printf "%s-%s" .Release.Name "mariadb" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Return the MariaDB Hostname
*/}}
{{- define "yii2Advanced.databaseHost" -}}
{{- if .Values.mariadb.enabled }}
    {{- printf "%s" (include "mariadb.fullname" .) -}}
{{- else -}}
    {{- printf "%s" .Values.externalDatabase.host -}}
{{- end -}}
{{- end -}}

{{/*
Return the MariaDB Port
*/}}
{{- define "yii2Advanced.databasePort" -}}
{{- if .Values.mariadb.enabled }}
    {{- printf "3306" -}}
{{- else -}}
    {{- printf "%d" (.Values.externalDatabase.port | int ) -}}
{{- end -}}
{{- end -}}

{{/*
Return the MariaDB Database Name
*/}}
{{- define "yii2Advanced.databaseName" -}}
{{- if .Values.mariadb.enabled }}
    {{- printf "%s" .Values.mariadb.db.name -}}
{{- else -}}
    {{- printf "%s" .Values.externalDatabase.database -}}
{{- end -}}
{{- end -}}

{{/*
Return the MariaDB User
*/}}
{{- define "yii2Advanced.databaseUser" -}}
{{- if .Values.mariadb.enabled }}
    {{- printf "%s" .Values.mariadb.db.user -}}
{{- else -}}
    {{- printf "%s" .Values.externalDatabase.user -}}
{{- end -}}
{{- end -}}

{{/*
Return the MariaDB password secret name
*/}}
{{- define "yii2Advanced.databaseSecretName" -}}
{{- if .Values.mariadb.enabled }}
    {{- printf "%s" (include "mariadb.fullname" .) -}}
{{- else -}}
    {{- printf "%s-%s" .Release.Name "externaldb" -}}
{{- end -}}
{{- end -}}

{{/*
Return the Redis Hostname
*/}}
{{- define "yii2Advanced.redisMasterHost" -}}
{{- if .Values.redis.enabled }}
    {{- printf "%s-redis-master" .Release.Name -}}
{{- else -}}
    {{- printf "%s" .Values.externalRedis.host -}}
{{- end -}}
{{- end -}}

{{/*
Return the Redis Hostname
*/}}
{{- define "yii2Advanced.redisReplicaHost" -}}
{{- if .Values.redis.enabled }}
    {{- printf "%s-redis-replica" .Release.Name -}}
{{- else -}}
    {{- printf "%s" .Values.externalRedis.host -}}
{{- end -}}
{{- end -}}

{{/*
Return the Redis Port
*/}}
{{- define "yii2Advanced.redisPort" -}}
{{- if .Values.redis.enabled }}
    {{- printf "6379" -}}
{{- else -}}
    {{- printf "%d" (.Values.externalRedis.port | int ) -}}
{{- end -}}
{{- end -}}

{{/*
Return the Redis password secret name
*/}}
{{- define "yii2Advanced.redisSecretName" -}}
{{- if .Values.redis.enabled }}
    {{- printf "%s-redis" .Release.Name -}}
{{- else -}}
    {{- printf "%s-%s" .Release.Name "externalredis" -}}
{{- end -}}
{{- end -}}

{{/*
Return the duplicity additional env variables
*/}}
{{- define "duplicity.additionalEnvVariables"}}
{{- range $key, $val := .Values.duplicity.additionalEnvVariables }}
- name: {{ $key }}
  value: {{ $val | quote }}
{{- end}}
{{- end }}