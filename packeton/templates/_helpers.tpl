{{/* vim: set filetype=mustache: */}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "packeton.mariadb.fullname" -}}
{{- printf "%s-%s" .Release.Name "mariadb" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Get the user defined LoadBalancerIP for this release.
Note, returns 127.0.0.1 if using ClusterIP.
*/}}
{{- define "packeton.serviceIP" -}}
{{- if eq .Values.service.type "ClusterIP" -}}
127.0.0.1
{{- else -}}
{{- .Values.service.loadBalancerIP | default "" -}}
{{- end -}}
{{- end -}}

{{/*
Gets the host to be used for this application.
If not using ClusterIP, or if a host or LoadBalancerIP is not defined, the value will be empty.
When using Ingress, it will be set to the Ingress hostname.
*/}}
{{- define "packeton.host" -}}
{{- if .Values.ingress.enabled }}
{{- $host := .Values.ingress.hostname | default "" -}}
{{- default (include "packeton.serviceIP" .) $host -}}
{{- else -}}
{{- $host := index .Values (printf "%sHost" .Chart.Name) | default "" -}}
{{- default (include "packeton.serviceIP" .) $host -}}
{{- end -}}
{{- end -}}

{{/*
Return the proper certificate image name
*/}}
{{- define "certificates.image" -}}
{{- include "common.images.image" ( dict "imageRoot" .Values.certificates.image "global" .Values.global ) -}}
{{- end -}}

{{/*
Return the proper packeton image name
*/}}
{{- define "packeton.web.image" -}}
{{- include "common.images.image" (dict "imageRoot" .Values.web.image "global" .Values.global) -}}
{{- end -}}

{{/*
Return the proper packeton image name
*/}}
{{- define "packeton.runner.image" -}}
{{- include "common.images.image" (dict "imageRoot" .Values.runner.image "global" .Values.global) -}}
{{- end -}}

{{/*
Return the proper image name (for the metrics image)
*/}}
{{- define "packeton.web.metrics.image" -}}
{{- include "common.images.image" (dict "imageRoot" .Values.web.metrics.image "global" .Values.global) -}}
{{- end -}}

{{/*
Return the proper image name (for the init container volume-permissions image)
*/}}
{{- define "packeton.volumePermissions.image" -}}
{{- include "common.images.image" ( dict "imageRoot" .Values.volumePermissions.image "global" .Values.global ) -}}
{{- end -}}

{{/*
Return the proper Docker Image Registry Secret Names
*/}}
{{- define "packeton.web.imagePullSecrets" -}}
{{- include "common.images.pullSecrets" (dict "images" (list .Values.web.image .Values.web.metrics.image) "global" .Values.global) -}}
{{- end -}}

{{/*
Return the proper Docker Image Registry Secret Names
*/}}
{{- define "packeton.runner.imagePullSecrets" -}}
{{- include "common.images.pullSecrets" (dict "images" (list .Values.runner.image) "global" .Values.global) -}}
{{- end -}}

{{/*
Return  the proper Storage Class
*/}}
{{- define "packeton.storageClass" -}}
{{- include "common.storage.class" (dict "persistence" .Values.persistence "global" .Values.global) -}}
{{- end -}}

{{/*
packeton credential secret name
*/}}
{{- define "packeton.secretName" -}}
{{- coalesce .Values.existingSecret (include "common.names.fullname" .) -}}
{{- end -}}

{{/*
Return the MariaDB Hostname
*/}}
{{- define "packeton.databaseHost" -}}
{{- if .Values.mariadb.enabled }}
    {{- if eq .Values.mariadb.architecture "replication" }}
        {{- printf "%s-%s" (include "packeton.mariadb.fullname" .) "primary" | trunc 63 | trimSuffix "-" -}}
    {{- else -}}
        {{- printf "%s" (include "packeton.mariadb.fullname" .) -}}
    {{- end -}}
{{- else -}}
    {{- printf "%s" .Values.externalDatabase.host -}}
{{- end -}}
{{- end -}}

{{/*
Return the MariaDB Port
*/}}
{{- define "packeton.databasePort" -}}
{{- if .Values.mariadb.enabled }}
    {{- printf "3306" -}}
{{- else -}}
    {{- printf "%d" (.Values.externalDatabase.port | int ) -}}
{{- end -}}
{{- end -}}

{{/*
Return the MariaDB Database Name
*/}}
{{- define "packeton.databaseName" -}}
{{- if .Values.mariadb.enabled }}
    {{- printf "%s" .Values.mariadb.db.name -}}
{{- else -}}
    {{- printf "%s" .Values.externalDatabase.database -}}
{{- end -}}
{{- end -}}

{{/*
Return the MariaDB User
*/}}
{{- define "packeton.databaseUser" -}}
{{- if .Values.mariadb.enabled }}
    {{- printf "%s" .Values.mariadb.db.user -}}
{{- else -}}
    {{- printf "%s" .Values.externalDatabase.user -}}
{{- end -}}
{{- end -}}

{{/*
Return the MariaDB Secret Name
*/}}
{{- define "packeton.databaseSecretName" -}}
{{- if .Values.mariadb.enabled }}
    {{- printf "%s" (include "packeton.mariadb.fullname" .) -}}
{{- else if .Values.externalDatabase.existingSecret -}}
    {{- printf "%s" .Values.externalDatabase.existingSecret -}}
{{- else -}}
    {{- printf "%s-%s" (include "common.names.fullname" .) "externaldb" -}}
{{- end -}}
{{- end -}}

{{/*
Create a default fully qualified app name
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "packeton.redis.fullname" -}}
{{- include "common.names.dependency.fullname" (dict "chartName" "redis" "chartValues" .Values.redis "context" $) -}}
{{- end -}}

{{- define "packeton.redis.host" -}}
{{- ternary (ternary (printf "%s-headless" (include "packeton.redis.fullname" .)) (printf "%s-master" (include "packeton.redis.fullname" .)) .Values.redis.sentinel.enabled) (ternary (printf "%s" .Values.externalRedis.sentinel.hosts) .Values.externalRedis.host .Values.externalRedis.sentinel.enabled) .Values.redis.enabled -}}
{{- end -}}

{{- define "packeton.redis.port" -}}
{{- ternary (ternary (int64 .Values.redis.sentinel.service.ports.sentinel) "6379" .Values.redis.sentinel.enabled) .Values.externalRedis.port .Values.redis.enabled -}}
{{- end -}}

{{- define "packeton.redis.sentinel.masterSet" -}}
{{- ternary (ternary (printf "%s" .Values.redis.sentinel.masterSet) ("") .Values.redis.sentinel.enabled) (ternary (printf "%s" .Values.externalRedis.sentinel.masterSet) ("") .Values.externalRedis.sentinel.enabled) .Values.redis.enabled -}}
{{- end -}}

{{- define "packeton.redis.databaseIndex" -}}
{{- ternary "0" .Values.externalRedis.databaseIndex .Values.redis.enabled -}}
{{- end -}}

{{/*
Return whether Redis&reg; uses password authentication or not
*/}}
{{- define "packeton.redis.auth.enabled" -}}
{{- if or .Values.redis.auth.enabled (and (not .Values.redis.enabled) .Values.externalRedis.password) }}
    {{- true -}}
{{- end -}}
{{- end -}}

{{- define "packeton.redis.rawPassword" -}}
  {{- if and (not .Values.redis.enabled) .Values.externalRedis.password -}}
    {{- .Values.externalRedis.password -}}
  {{- end -}}
  {{- if and .Values.redis.enabled .Values.redis.auth.password .Values.redis.auth.enabled -}}
    {{- .Values.redis.auth.password -}}
  {{- end -}}
{{- end -}}

{{- define "packeton.redis.escapedRawPassword" -}}
  {{- if (include "packeton.redis.rawPassword" . ) -}}
    {{- include "packeton.redis.rawPassword" . | urlquery | replace "+" "%20" -}}
  {{- end -}}
{{- end -}}


{{- define "packeton.redis" -}}
  {{- if and (eq .Values.externalRedis.sentinel.enabled false) (eq .Values.redis.sentinel.enabled false) -}}
    {{- if (include "packeton.redis.escapedRawPassword" . ) -}}
      {{- printf "redis://default:%s@%s:%s/%s" (include "packeton.redis.escapedRawPassword" . ) (include "packeton.redis.host" . ) (include "packeton.redis.port" . ) (include "packeton.redis.databaseIndex" . ) -}}
    {{- else -}}
      {{- printf "redis://%s:%s/%s" (include "packeton.redis.host" . ) (include "packeton.redis.port" . ) (include "packeton.redis.databaseIndex" . ) -}}
    {{- end -}}
  {{- else -}}
    {{- if (include "packeton.redis.escapedRawPassword" . ) -}}
      {{- printf "redis+sentinel://default:%s@%s:%s/%s/%s" (include "packeton.redis.escapedRawPassword" . ) (include "packeton.redis.host" . ) (include "packeton.redis.port" . ) (include "packeton.redis.sentinel.masterSet" . ) (include "packeton.redis.databaseIndex" . ) -}}
    {{- else -}}
      {{- printf "redis+sentinel://%s:%s/%s/%s" (include "packeton.redis.host" . ) (include "packeton.redis.port" . ) (include "packeton.redis.sentinel.masterSet" . ) (include "packeton.redis.databaseIndex" . ) -}}
    {{- end -}}
  {{- end -}}
{{- end -}}