{{/* vim: set filetype=mustache: */}}

{{/*
Create a default fully qualified app name for endpoint.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "basicContainer.endpointname" -}}
{{- printf "%s" (include "common.names.fullname" .global) | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "basicContainer.containername" -}}
{{- printf "yii2-%s" (include "common.names.fullname" .global) | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Return the proper Docker Image Registry Secret Names
*/}}
{{- define "basicContainer.imagePullSecrets" -}}
{{- include "common.images.pullSecrets" (dict "images" (list .Values.image .Values.metrics.image .Values.volumePermissions.image) "global" .Values.global) -}}
{{- end -}}

{{/*
 Create the name of the service account to use
 */}}
{{- define "basicContainer.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "common.names.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Return the proper Basic container image name
*/}}
{{- define "basicContainer.image" -}}
{{- include "common.images.image" (dict "imageRoot" .Values.image "global" .Values.global) -}}
{{- end -}}

{{/*
REDIS
*/}}
{{- define "basicContainer.valkey.fullname" -}}
{{- printf "%s-valkey" (default .Release.Name .Values.valkey.nameOverride) -}}
{{- end -}}

{{- define "basicContainer.valkey.url" -}}
{{- $scheme := .scheme | default "valkey" -}}
{{- if .valkey.enabled -}}
{{- if .valkey.sentinel.enabled -}}
{{- $port := printf "%v" (.valkey.sentinel.service.port | default 26379) -}}
{{- printf "%s://@%s:%s" $scheme (include "basicContainer.valkey.fullname" .context) $port -}}
{{- else -}}
{{- $port := printf "%v" (.valkey.primary.service.port | default 6379) -}}
{{- printf "%s://%s-primary:%s" $scheme (include "basicContainer.valkey.fullname" .context) $port -}}
{{- end -}}
{{- else if .externalRedis.host -}}
{{- $port := printf "%v" .externalRedis.port -}}
{{- $databaseIndex := printf "%v" .externalRedis.databaseIndex -}}
{{- printf "%s://%s:%s/%s" $scheme .externalRedis.host $port $databaseIndex -}}
{{- end -}}
{{- end -}}

{{- define "basicContainer.valkey.sentinelUrl" -}}
{{- $port := printf "%v" .valkey.sentinel.service.sentinelPort -}}
{{- $scheme := .scheme | default "valkey" -}}
{{- printf "%s://%s:%s" $scheme (include "basicContainer.valkey.fullname" .context) $port -}}
{{- end -}}

{{/*
Get the password secret.
*/}}
{{- define "basicContainer.valkey.secretName" -}}
{{- if .Values.valkey.enabled }}
{{- if .Values.valkey.auth.existingSecret -}}
{{- .Values.valkey.auth.existingSecret -}}
{{- else -}}
{{- include "basicContainer.valkey.fullname" . -}}
{{- end -}}
{{- else if .Values.externalValkey.existingSecret }}
{{- .Values.externalValkey.existingSecret -}}
{{- else -}}
{{- printf "%s-external-valkey" (include "common.names.fullname" .) -}}
{{- end -}}
{{- end -}}

{{/*
Get the password key to be retrieved from Redis&reg; secret.
*/}}
{{- define "basicContainer.valkey.secretPasswordKey" -}}
{{- if and .Values.valkey.enabled .Values.valkey.auth.existingSecret .Values.valkey.auth.existingSecretPasswordKey -}}
{{- printf "%s" .Values.valkey.auth.existingSecretPasswordKey -}}
{{- else if and (not .Values.valkey.enabled) .Values.externalValkey.existingSecret .Values.externalValkey.existingSecretPasswordKey -}}
{{- printf "%s" .Values.externalValkey.existingSecretPasswordKey -}}
{{- else -}}
{{- printf "valkey-password" -}}
{{- end -}}
{{- end -}}

{{/*
MARIADB
*/}}
{{- define "basicContainer.mariadb.fullname" -}}
{{- include "common.names.dependency.fullname" (dict "chartName" "mariadb" "chartValues" .Values.mariadb "context" $) -}}
{{- end -}}

{{/*
Return the MariaDB Hostname
*/}}
{{- define "basicContainer.mariadb.host" -}}
{{- if .Values.mariadb.enabled }}
    {{- if eq .Values.mariadb.architecture "replication" }}
        {{- printf "%s-primary" (include "basicContainer.mariadb.fullname" .) | trunc 63 | trimSuffix "-" -}}
    {{- else -}}
        {{- printf "%s" (include "basicContainer.mariadb.fullname" .) -}}
    {{- end -}}
{{- else -}}
    {{- printf "%s" .Values.externalDatabase.host -}}
{{- end -}}
{{- end -}}

{{/*
Return the MariaDB Port
*/}}
{{- define "basicContainer.mariadb.port" -}}
{{- if .Values.mariadb.enabled }}
    {{- printf "3306" -}}
{{- else -}}
    {{- printf "%d" (.Values.externalDatabase.port | int ) -}}
{{- end -}}
{{- end -}}

{{/*
Return the MariaDB Database Name
*/}}
{{- define "basicContainer.mariadb.name" -}}
{{- if .Values.mariadb.enabled }}
    {{- printf "%s" .Values.mariadb.db.name -}}
{{- else -}}
    {{- printf "%s" .Values.externalDatabase.database -}}
{{- end -}}
{{- end -}}

{{/*
Return the MariaDB User
*/}}
{{- define "basicContainer.mariadb.user" -}}
{{- if .Values.mariadb.enabled }}
    {{- printf "%s" .Values.mariadb.db.user -}}
{{- else -}}
    {{- printf "%s" .Values.externalDatabase.user -}}
{{- end -}}
{{- end -}}

{{/*
Return the MariaDB Secret Name
*/}}
{{- define "basicContainer.mariadb.secretName" -}}
{{- if .Values.mariadb.enabled }}
    {{- if .Values.mariadb.existingSecret -}}
        {{- printf "%s" .Values.mariadb.existingSecret -}}
    {{- else -}}
        {{- printf "%s" (include "basicContainer.mariadb.fullname" .) -}}
    {{- end -}}
{{- else if .Values.externalDatabase.existingSecret -}}
    {{- include "common.tplvalues.render" (dict "value" .Values.externalDatabase.existingSecret "context" $) -}}
{{- else -}}
    {{- printf "%s-externaldb" (include "common.names.fullname" .) -}}
{{- end -}}
{{- end -}}

