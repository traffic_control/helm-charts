
To use this helm registry, add this registry to your helm charts:
```
helm repo add traffic-control https://gitlab.com/api/v4/projects/23957858/packages/helm/production
```