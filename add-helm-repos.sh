#!/bin/sh

for repo in $HELM_REPOS ; do
    MD5SUM=`echo $repo | md5sum`
    helm repo add ${MD5SUM:0:16} $repo
done