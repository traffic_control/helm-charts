#!/bin/sh

for d in */ ; do
    helm cm-push $d $1
done